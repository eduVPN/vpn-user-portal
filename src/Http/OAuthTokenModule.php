<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Http;

use fkooman\OAuth\Server\Exception\OAuthException;
use fkooman\OAuth\Server\OAuthServer;
use Vpn\Portal\LoggerInterface;

class OAuthTokenModule implements ServiceModuleInterface
{
    private OAuthServer $oauthServer;
    private LoggerInterface $logger;

    public function __construct(OAuthServer $oauthServer, LoggerInterface $logger)
    {
        $this->oauthServer = $oauthServer;
        $this->logger = $logger;
    }

    public function init(ServiceInterface $service): void
    {
        $service->postBeforeAuth(
            '/oauth/token',
            function (Request $request): Response {
                try {
                    $tokenResponse = $this->oauthServer->postToken();

                    return new Response($tokenResponse->getBody(), $tokenResponse->getHeaders(), $tokenResponse->getStatusCode());
                } catch (OAuthException $e) {
                    $this->logger->warning(\sprintf('OAuth (error=%s description=%s]', $e->getMessage(), $e->getDescription() ?? 'N/A'));
                    $jsonResponse = $e->getJsonResponse();

                    return new Response($jsonResponse->getBody(), $jsonResponse->getHeaders(), $jsonResponse->getStatusCode());
                }
            }
        );
    }
}
