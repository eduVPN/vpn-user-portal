<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Http;

use fkooman\OAuth\Server\AccessToken;

class ApiUserInfo
{
    private string $userId;
    private AccessToken $accessToken;

    public function __construct(string $userId, AccessToken $accessToken)
    {
        $this->userId = $userId;
        $this->accessToken = $accessToken;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function accessToken(): AccessToken
    {
        return $this->accessToken;
    }
}
