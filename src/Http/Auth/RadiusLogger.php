<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Http\Auth;

use fkooman\Radius\LoggerInterface as RadiusLoggerInterface;
use Vpn\Portal\LoggerInterface as VpnLoggerInterface;

class RadiusLogger implements RadiusLoggerInterface
{
    private VpnLoggerInterface $vpnLogger;

    public function __construct(VpnLoggerInterface $vpnLogger)
    {
        $this->vpnLogger = $vpnLogger;
    }

    public function error(string $logMessage): void
    {
        $this->vpnLogger->error(\sprintf('[RADIUS] %s', $logMessage));
    }

    public function warning(string $logMessage): void
    {
        $this->vpnLogger->warning(\sprintf('[RADIUS] %s', $logMessage));
    }

    public function info(string $logMessage): void
    {
        $this->vpnLogger->info(\sprintf('[RADIUS] %s', $logMessage));
    }

    public function debug(string $logMessage): void
    {
        // we do NOT log any debugging
    }
}
