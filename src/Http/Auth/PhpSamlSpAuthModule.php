<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Http\Auth;

use fkooman\SAML\SP\Api\AuthOptions;
use fkooman\SAML\SP\Api\SamlAuth;
use RuntimeException;
use Vpn\Portal\Cfg\PhpSamlSpAuthConfig;
use Vpn\Portal\Http\Exception\HttpException;
use Vpn\Portal\Http\RedirectResponse;
use Vpn\Portal\Http\Request;
use Vpn\Portal\Http\Response;
use Vpn\Portal\Http\UserInfo;

class PhpSamlSpAuthModule extends AbstractAuthModule
{
    private PhpSamlSpAuthConfig $config;
    private SamlAuth $samlAuth;

    public function __construct(PhpSamlSpAuthConfig $config)
    {
        $this->config = $config;
        $this->samlAuth = new SamlAuth();

        // we require php-saml-sp >= 2.4.0, but as it comes from a separate
        // repository, we can't really guarantee that the admins updated it
        // in lock step with vpn-user-portal, at least now we get to yell about
        // it...
        if (!method_exists($this->samlAuth, 'getLogoutURL')) {
            throw new RuntimeException('this version of vpn-user-portal requires php-saml-sp >= 2.4.0, please upgrade, see https://www.php-saml-sp.eu/');
        }
    }

    public function userInfo(Request $request): ?UserInfo
    {
        if (!$this->samlAuth->isAuthenticated($this->getAuthOptions())) {
            return null;
        }
        $samlAssertion = $this->samlAuth->getAssertion($this->getAuthOptions());
        if (null === $userId = $samlAssertion->getFirstAttributeValue($this->config->userIdAttribute())) {
            throw new HttpException(\sprintf('missing "userIdAttribute" attribute "%s" in SAML assertion', $this->config->userIdAttribute()), 500);
        }

        return new UserInfo(
            $userId,
            self::flattenPermissionList($samlAssertion->getAttributes(), $this->config->permissionAttributeList())
        );
    }

    public function startAuth(Request $request): ?Response
    {
        return new RedirectResponse($this->samlAuth->getLoginURL($this->getAuthOptions()));
    }

    public function triggerLogout(Request $request): Response
    {
        return new RedirectResponse($this->samlAuth->getLogoutURL($request->requireReferrer()));
    }

    private function getAuthOptions(): ?AuthOptions
    {
        if (null === $authnContext = $this->config->authnContext()) {
            return null;
        }

        return AuthOptions::init()->withAuthnContextClassRef($authnContext);
    }
}
