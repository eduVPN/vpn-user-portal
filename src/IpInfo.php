<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal;

class IpInfo
{
    private string $countryCode;
    private string $geoUri;

    public function __construct(string $countryCode, string $geoUri)
    {
        $this->countryCode = $countryCode;
        $this->geoUri = $geoUri;
    }

    public function countryCode(): string
    {
        return $this->countryCode;
    }

    public function geoUri(): string
    {
        return $this->geoUri;
    }
}
