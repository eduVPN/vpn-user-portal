<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal;

class NodeInfo
{
    private int $nodeNumber;
    private string $nodeUrl;
    private string $hostName;

    public function __construct(int $nodeNumber, string $nodeUrl, string $hostName)
    {
        $this->nodeNumber = $nodeNumber;
        $this->nodeUrl = $nodeUrl;
        $this->hostName = $hostName;
    }

    public function nodeNumber(): int
    {
        return $this->nodeNumber;
    }

    public function nodeUrl(): string
    {
        return $this->nodeUrl;
    }

    public function hostName(): string
    {
        return $this->hostName;
    }
}
