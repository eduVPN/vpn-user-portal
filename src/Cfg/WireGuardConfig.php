<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Cfg;

class WireGuardConfig
{
    use ConfigTrait;

    private array $configData;

    public function __construct(array $configData)
    {
        $this->configData = $configData;
    }

    public function toArray(): array
    {
        return $this->configData;
    }

    public function setMtu(): ?int
    {
        return $this->optionalInt('setMtu');
    }

    public function listenPort(): int
    {
        return $this->requireInt('listenPort', 51820);
    }

    /**
     * Only support the WireGuard TCP Proxy, i.e. no WireGuard UDP support is
     * offered by the server.
     */
    public function onlyProxy(): bool
    {
        return $this->requireBool('onlyProxy', false);
    }

    public function enableProxy(): bool
    {
        return $this->requireBool('enableProxy', false);
    }

    public function proxyUrl(): ?string
    {
        return $this->optionalString('proxyUrl');
    }

    /**
     * Allow users to click the "Extend" button for manual WireGuard
     * configuration file downloads to not require loading a new WireGuard
     * configuration file in their client.
     */
    public function allowExpiryExtension(): bool
    {
        return $this->requireBool('allowExpiryExtension', false);
    }

    /**
     * Get a list of configuration keys NOT supported in order to warn the
     * admins on the "Info" page about it.
     *
     * @return array<string>
     */
    public function unsupportedConfigKeys(): array
    {
        $supportedKeys = [
            'setMtu',
            'listenPort',
            'onlyProxy',
            'enableProxy',
            'proxyUrl',
            'allowExpiryExtension',
        ];

        $unsupportedKeys = [];
        foreach (array_keys($this->configData) as $configKey) {
            if (!\in_array($configKey, $supportedKeys, true)) {
                $unsupportedKeys[] = $configKey;
            }
        }

        return $unsupportedKeys;
    }
}
