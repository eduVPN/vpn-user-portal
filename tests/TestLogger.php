<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use Vpn\Portal\LoggerInterface;

class TestLogger implements LoggerInterface
{
    /** @var array<string> */
    private array $logMessages;

    public function warning(string $logMessage): void
    {
        $this->logMessages[] = '[W] ' . $logMessage;
    }

    public function error(string $logMessage): void
    {
        $this->logMessages[] = '[E] ' . $logMessage;
    }

    public function info(string $logMessage): void
    {
        $this->logMessages[] = '[I] ' . $logMessage;
    }

    public function debug(string $logMessage): void
    {
        $this->logMessages[] = '[D] ' . $logMessage;
    }

    /**
     * @return array<string>
     */
    public function getAll(): array
    {
        return $this->logMessages;
    }
}
