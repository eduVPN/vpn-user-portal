<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\GeoIp;

/**
 * Test our GeoIp class.
 *
 * We got the test data from https://github.com/maxmind/MaxMind-DB/ which
 * includes tiny databases. The test data, in "test-data" is generated from
 * the "source-data" folder, so you can check the "contents" of the database
 * as JSON.
 *
 * @covers \Vpn\Portal\GeoIp
 *
 * @uses \Vpn\Portal\IpInfo
 * @uses \Vpn\Portal\FileIO
 */
final class GeoIpTest extends TestCase
{
    public function testCity(): void
    {
        if (\extension_loaded('maxminddb')) {
            $geoIp = new GeoIp(__DIR__ . '/data/GeoIP2-City-Test.mmdb');
            $ipInfo = $geoIp->get('2001:218::1');
            $this->assertSame('JP', $ipInfo->countryCode());
            $this->assertSame('geo:35.685360,139.753090', $ipInfo->geoUri());

            return;
        }
        $this->markTestSkipped();
    }

    public function testPrivate(): void
    {
        if (\extension_loaded('maxminddb')) {
            $geoIp = new GeoIp(__DIR__ . '/data/GeoIP2-City-Test.mmdb');
            $this->assertNull($geoIp->get('192.168.1.1'));

            return;
        }
        $this->markTestSkipped();
    }
}
