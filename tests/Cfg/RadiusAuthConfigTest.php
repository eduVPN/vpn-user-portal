<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Cfg\RadiusAuthConfig;

/**
 * @covers \Vpn\Portal\Cfg\RadiusAuthConfig
 */
final class RadiusAuthConfigTest extends TestCase
{
    public function testIpFour(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    '127.0.0.1:1812:s3cr3t',
                ],
            ]
        );
        $this->assertCount(1, $r->serverList());
        $serverInfo = $r->serverList()[0];
        $this->assertSame('udp://127.0.0.1:1812', $serverInfo->serverUri());
        $this->assertSame('s3cr3t', $serverInfo->sharedSecret());
    }

    public function testIpSix(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    '[fdd4:56b7:5ebc:4e82::1]:1812:s3cr3t',
                ],
            ]
        );
        $this->assertCount(1, $r->serverList());
        $serverInfo = $r->serverList()[0];
        $this->assertSame('udp://[fdd4:56b7:5ebc:4e82::1]:1812', $serverInfo->serverUri());
        $this->assertSame('s3cr3t', $serverInfo->sharedSecret());
    }

    public function testMultiple(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    'radius.example.org:12345:foobar',
                    '127.0.0.1:1812:s3cr3t',
                    '[fdd4:56b7:5ebc:4e82::1]:1812:s3cr3t',
                ],
            ]
        );
        $this->assertCount(3, $r->serverList());
        $this->assertSame('udp://radius.example.org:12345', $r->serverList()[0]->serverUri());
        $this->assertSame('foobar', $r->serverList()[0]->sharedSecret());
        $this->assertSame('udp://127.0.0.1:1812', $r->serverList()[1]->serverUri());
        $this->assertSame('s3cr3t', $r->serverList()[1]->sharedSecret());
        $this->assertSame('udp://[fdd4:56b7:5ebc:4e82::1]:1812', $r->serverList()[2]->serverUri());
        $this->assertSame('s3cr3t', $r->serverList()[2]->sharedSecret());
    }

    public function testRealm(): void
    {
        $r = new RadiusAuthConfig(['addRealm' => 'example.org']);
        $this->assertSame('example.org', $r->radiusRealm());
        $r = new RadiusAuthConfig(['radiusRealm' => 'example.org']);
        $this->assertSame('example.org', $r->radiusRealm());
        $r = new RadiusAuthConfig([]);
        $this->assertNull($r->radiusRealm());
    }
}
