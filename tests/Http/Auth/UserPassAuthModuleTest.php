<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Http\Auth\UserPassAuthModule;

/**
 * @coversNothing
 */
class UserPassAuthModuleTest extends TestCase
{
    public function testSanitize(): void
    {
        $this->assertSame('foo', UserPassAuthModule::sanitizeUserId('foo'));
        $this->assertSame('foo_', UserPassAuthModule::sanitizeUserId('foo*'));
        $this->assertSame('_foo_', UserPassAuthModule::sanitizeUserId('*foo*'));
        $this->assertSame('_foo_foo_', UserPassAuthModule::sanitizeUserId('*foo*foo*'));
        $this->assertSame('foo__bar', UserPassAuthModule::sanitizeUserId("foo\n\nbar"));
        $this->assertSame('foo.bar', UserPassAuthModule::sanitizeUserId("foo.bar"));
        $this->assertSame('foo..bar', UserPassAuthModule::sanitizeUserId("foo..bar"));
        $this->assertSame('x_y', UserPassAuthModule::sanitizeUserId("x_y"));
        $this->assertSame('x-y-z', UserPassAuthModule::sanitizeUserId("x-y-z"));
        $this->assertSame('a_b', UserPassAuthModule::sanitizeUserId('a\b'));
        $this->assertSame('.._foo_bar', UserPassAuthModule::sanitizeUserId('../foo/bar'));
        $this->assertSame('_script_window.alert__x_____script_', UserPassAuthModule::sanitizeUserId('<script>window.alert("x");</script>'));
    }
}
