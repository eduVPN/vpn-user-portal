<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use RangeException;
use Vpn\Portal\Http\Exception\HttpException;
use Vpn\Portal\Http\Request;

/**
 * @covers \Vpn\Portal\Http\Request
 *
 * @uses \Vpn\Portal\Http\Exception\HttpException
 */
final class RequestTest extends TestCase
{
    public function testValidate(): void
    {
        $r = new Request(
            [],
            [],
            [
                'xyz' => 'foo',
            ],
            []
        );

        static::expectException(HttpException::class);
        static::expectExceptionMessage('invalid value for "xyz"');
        $r->requirePostParameter('xyz', function ($s): void {
            throw new RangeException();
        });
    }

    public function testRequireAnyHeaderNoHeader(): void
    {
        $r = new Request([], [], [], []);
        static::expectException(HttpException::class);
        static::expectExceptionMessage('missing request header [FOO,BAR]');
        $r->requireAnyHeader(['FOO','BAR']);
    }

    public function testRequireAnyHeaderFirstHeader(): void
    {
        $r = new Request(['FOO' => 'bar'], [], [], []);
        $this->assertSame('bar', $r->requireAnyHeader(['FOO','BAR']));
    }

    public function testRequireAnyHeaderSecondHeader(): void
    {
        $r = new Request(['BAR' => 'baz'], [], [], []);
        $this->assertSame('baz', $r->requireAnyHeader(['FOO','BAR']));
    }

    public function testRequireAnyHeaderBoth(): void
    {
        $r = new Request(['FOO' => 'bar', 'BAR' => 'baz'], [], [], []);
        $this->assertSame('baz', $r->requireAnyHeader(['BAR', 'FOO']));
    }
}
