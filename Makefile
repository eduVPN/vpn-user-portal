HTTP_PORT ?= 8082
SRC_DIRS=src web libexec bin

.PHONY: test cov fmt psalm phpstan sloc dev

test:
	# try system wide installations of PHPUnit first
	/usr/bin/phpunit || /usr/bin/phpunit9 || phpunit

cov:
	/usr/bin/phpunit --coverage-html cov || /usr/bin/phpunit9 --coverage-html cov || phpunit --coverage-html cov

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	tokei $(SRC_DIRS) || cloc $(SRC_DIRS)

dev:
	@php -S localhost:$(HTTP_PORT) -t web dev/router.php
