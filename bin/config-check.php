<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2025, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

require_once \dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = \dirname(__DIR__);

use Vpn\Portal\Cfg\Config;
use Vpn\Portal\ConfigCheck;

try {
    $config = Config::fromFile($baseDir . '/config/config.php');

    $configStatus = ConfigCheck::verify($config);
    $globalProblems = $configStatus['global_problems'];
    $profileProblems = $configStatus['profile_problems'];

    $problemMessageList = [];
    foreach ($globalProblems as $p) {
        $problemMessageList[] = \sprintf('[SERVER]: %s', $p);
    }
    foreach ($profileProblems as $profileId => $problemList) {
        foreach ($problemList as $p) {
            $problemMessageList[] = \sprintf('[PROFILE=%s]: %s', $profileId, $p);
        }
    }

    if (0 !== \count($problemMessageList)) {
        echo '********************************************************************' . \PHP_EOL;
        echo '* WARNING: Possible issues with your VPN configuration were found! *' . \PHP_EOL;
        echo '********************************************************************' . \PHP_EOL;
        foreach ($problemMessageList as $problemMessage) {
            echo $problemMessage . \PHP_EOL;
        }

        exit(1);
    }
} catch (Throwable $e) {
    echo \sprintf('ERROR: %s', $e->getMessage()) . \PHP_EOL;

    exit(1);
}
